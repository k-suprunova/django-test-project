import itertools

# 1. В Питоне нет класса frange, который бы работал с float. Создать свою версию такого класса,
# который бы поддерживал интерфейс стандартного range, но работал при этом с float.
print('Task 1')


class Frange:

    def __init__(self, start, end=None, step=None):
        self.start = start
        self.end = end
        self.step = step

        if self.end is None:
            self.end = self.start + 0.0
            self.start = 0.0

        if self.step is None:
            self.step = 1.0

    def __iter__(self):
        count = []
        while True:
            temp = self.start + len(count) * self.step
            if self.step > 0 and temp >= self.end:
                break
            elif self.step < 0 and temp <= self.end:
                break
            count.append(temp)
        return iter(count)

    @staticmethod
    def assert_test():
        assert (list(frange(5)) == [0, 1, 2, 3, 4])
        assert (list(frange(2, 5)) == [2, 3, 4])
        assert (list(frange(2, 10, 2)) == [2, 4, 6, 8])
        assert (list(frange(10, 2, -2)) == [10, 8, 6, 4])
        assert (list(frange(2, 5.5, 1.5)) == [2, 3.5, 5])
        assert (list(frange(1, 5)) == [1, 2, 3, 4])
        assert (list(frange(0, 5)) == [0, 1, 2, 3, 4])
        assert (list(frange(0, 0)) == [])
        assert (list(frange(100, 0)) == [])
        # assert (list(itertools.islice(frange(0, float(10 ** 10), 1.0), 0, 4)) == [0, 1.0, 2.0, 3.0])

        return 'SUCCESS!'


frange = Frange

for i in frange(1, 100, 3.5):
    print(i)

print(frange.assert_test())


# 2. Реализовать imap, который полностью совместим со стандартным map.
# Придумать минимальное тестовое покрытие по аналогии с предыдущим заданием,
# которое гарантирует корректную работоспособнность и совместимость со стандартным map.
print('-'*60)
print('Task 2')


class Imap:

    def __init__(self, func, *obj):
        self.func = func
        self.obj = obj

        if self.func is None or self.obj is None:
            raise TypeError('Mapper should have two or more parameters')

    def __iter__(self):
        result = []
        if len(self.obj) > 0:
            min_len = min(len(subsequence) for subsequence in self.obj)
            for j in range(min_len):
                result.append(self.func(*[subsequence[j] for subsequence in self.obj]))
        return iter(result)

    @staticmethod
    def assert_test():
        # 1
        assert (list(imap(str.upper, ['a', 'b', 'c'])) == ['A', 'B', 'C'])
        # 2
        circle_areas = [3.56773, 5.57668, 4.00914, 56.24241, 9.01344, 32.00013]
        assert (list(imap(round, circle_areas)) == [4, 6, 4, 56, 9, 32])
        # 3
        nums = [1, 2, 3, 4, 5]
        nums2 = [100, 200, 300, 400, 500]
        assert (list(imap(lambda x, y: x*y, nums, nums2)) == [100, 400, 900, 1600, 2500])

        return 'SUCCESS!'


imap = Imap
print(imap.assert_test())
