import random
print('-' * 10 + '\n' + 'Task 1')

# В иерархии графических фигур, рассмотренной в классе, реализовать метод square() для каждой
# из отнаследованных фигур. Метод определяет площадь соответствующей фигуры.?


class Shape:

    """Basic shape class that describes basic shape things like rendering, shaping etc."""

    def __init__(self, x, y):
        self.x = x
        self.y = y

    def render(self, color):
        """Returns colour with which the shape was rendered."""
        print(f'Rendered with color {color}')

    def scale(self, scale_factor):
        """Returns scaling"""
        print(f'Scaled with {scale_factor}')

    def square(self, square_res):
        """Returns square of a shape"""
        print(f'Square of {self.__name__} is {square_res}')

    def centre(self):
        """Returns the center of a shape by using given coordinates."""
        print(self.x, self.y)


class Circle(Shape):

    CONST = 3.14

    def __init__(self, x, y, radius):
        super().__init__(x, y)
        self.radius = radius

    def square(self):
        print(self.CONST * (int(self.radius) ** 2))


class Rectangle(Shape):

    def __init__(self, x, y, height, width):
        super().__init__(x, y)
        self.height = height
        self.width = width

    def print_sides(self):
        print(self.height, self.width)

    def square(self):
        print((int(self.height) * int(self.width)))


class Triangle(Shape):

    CONST = (1/2)

    def __init__(self, x, y, a, h):
        super().__init__(x, y)
        self.a = a
        self.h = h

    def print_sides(self):
        print(self.a, self.h)

    def square(self):
        print(self.CONST * self.a * self.h)


c = Circle(4, 2, 42)
c.square()

s = Rectangle(1, 10, 20, 30)
s.square()

t = Triangle(1, 2, 10, 13)
t.square()


print('-' * 10 + '\n' + 'Task 2')

# Создать класс Robot и его потомков - классы SpotMini, Atlas и Handle.
# В родительском классе должно быть определен конструктор и как минимум 3 атрибута и 1 метод.
# В классах-потомках должны быть добавлены минимум по 1 новому методу и по 1 новому атрибуту.


class Robot:
    """Basic Robot class that takes colour, name and purpose as args and can return welcoming."""

    def __init__(self, colour, name, purpose):
        self.colour = colour
        self.name = name
        self.purpose = purpose

    def welcoming(self):
        """Returns welcoming from a robot using colour, name and purpose."""
        return f'Hello! My name is {self.name}. I\'m a {self.colour} robot that does {self.purpose}.'


class SportMini(Robot):
    """SportMini's distinctive character is its number, that's being taken as an argument."""

    def __init__(self, colour, name, purpose, number):
        super().__init__(colour, name, purpose)
        self.number = number

    def work(self):
        """Returns SportMini's purpose with its number."""
        return f'I do {self.purpose} by {self.number} number.'


class Atlas(Robot):
    """Atlas' distinctive character is its design, that's being taken as a new argument."""

    def __init__(self, colour, name, purpose, design):
        super().__init__(colour, name, purpose)
        self.design = design

    def my_design(self):
        """Returns Atlas' design."""
        return f'My design is {self.design}.'


class Handle(Robot):
    """Handle's distinctive character is its limb type, that's being taken as a new argument."""

    def __init__(self, colour, name, purpose, limb_type):
        super().__init__(colour, name, purpose)
        self.limb_type = limb_type

    def my_limb_type(self):
        """Returns Handle's limb type."""
        return f'I\'m Handle and I can handle things using my {self.limb_type}.'


def robot_test():
    sport_mini = SportMini("steal", "Ticky", "racing", 2901902)
    atlas = Atlas("black", "Al", "test", "new")
    handle = Handle("white", "HYBE", "handling things", "tail")
    print(sport_mini.welcoming(), sport_mini.work(), atlas.my_design(), handle.my_limb_type())


robot_test()
