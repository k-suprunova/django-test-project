import os, sqlite3, requests


def parse_int(request, param_name, default, min=float('-inf'), max=float('inf')):
    value = request.GET.get(param_name, str(default))

    if not value.isdigit():
        raise ValueError(f'Error: int is expected for {param_name} param')

    value = int(value)

    if not (min <= value <= max):
        raise ValueError(f'Error: {param_name} is out of range: should be between {min} and {max}')

    return value


def parse_bool(request, param_name, default):
    value = request.GET.get(param_name, str(int(default)))

    if not value.isdigit():
        raise ValueError(f'Error: int is expected for {param_name} param')

    value = int(value)

    if not (0 <= value <= 1):
        raise ValueError(f'Error: {param_name} should be boolean (0 or 1)')

    return value


def parse_str(request, param_name):
    value = request.GET.get(param_name)

    if not str(value).isalpha():
        raise ValueError(f'Error: str is expected for {param_name} param')
    if value is not None:
        value = str(value)
    return value


def parse_currency(request, param_name, currency):
    value = request.args.get(param_name, str(currency))

    if not len(value) == 3 or not str(value).isalpha():
        raise ValueError(f'Error: {param_name} should be 3 character string (example: USD).')

    return value


def request_handler(url):
    try:
        res = requests.get(url)
    except Exception as ex:
        raise Exception(f'Error: Wrong request url was provided or something went wrong while '
                        f'calling this request: {url}. {ex}')
    if res.ok:
        try:
            return res.json()
        except Exception as ex:
            raise Exception(f'Response from {url} was received not in JSON format. {res.content}. {ex}')
    else:
        raise Exception(
            f'Error: Unsuccessful request, please try again. {res.reason}')


def execute_query(query, db, args=()):
    try:
        db_path = os.path.join(os.getcwd(), db)
        conn = sqlite3.connect(db_path)
        cur = conn.cursor()
    except Exception as ex:
        raise Exception(f'Something went wrong during db connection. {ex}')
    try:
        cur.execute(query, args)
        conn.commit()
        records = cur.fetchall()
        return records
    except Exception as ex:
        raise Exception(f'Something went wrong during query execution. {ex}')
