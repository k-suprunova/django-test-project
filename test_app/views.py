from django.http import HttpResponse
from django.shortcuts import render
from test_app.utils import execute_query, parse_int

# Create your views here.


def index(request):
    return HttpResponse('Hello World!')


def get_sales(request):
    """
    returns company's sales as the sum of all purchases | Tables -> Invoice_Items
    """
    db = 'chinook.db'
    query = 'select sum(UnitPrice * Quantity) as purchase_sum from Invoice_Items'
    res = execute_query(query, db)
    fin_res = str(res[0][0])
    return HttpResponse(f"Company's sales are {fin_res}.")


def get_genres(request):
    """
    returns total length of tracks in seconds, grouped by music genre | Tables -> tracks; tracks
    """
    db = 'chinook.db'
    query = """
    select sum(t.Milliseconds)/(1000) as duration_in_sec, g.name
from tracks t 
join genres g
on t.GenreId = g.GenreId
group by g.name
order by  duration_in_sec desc
    """
    res = execute_query(query, db)
    fin_res = []
    for i in res:
        fin_res.append(f'Total length of {i[1]} music in the album is {i[0]} seconds.<br>')
    return HttpResponse(fin_res)


def get_greatest_hits(request):
    """
    returns top selling tracks with total sales. count - optional parameter,
    if not specified - returns all results | Tables -> Invoices; InvoiceId; Tracks
    """
    db = 'chinook.db'
    query = """
    select i.Total, t.name
from invoices i 
join invoice_items ii 
on i.InvoiceId = ii.InvoiceId 
join tracks t
on ii.TrackId = t.TrackId
group by t.name
order by  i.Total DESC
    """
    param_name = 'count'
    value = parse_int(request, param_name, 2000)
    if value:
        query += f' LIMIT {value}'
    res = execute_query(query, db)
    intro = f'<p><b>Here is a top {value} list of greatest selling tracks</b></p>'
    fin_res = [intro]
    for num, i in enumerate(res, 1):
        fin_res.append(f'#{num}:"{i[1]}" with total sales of {i[0]} USD.<br>')
    return HttpResponse(fin_res)
