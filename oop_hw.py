print('Task 1')


class Shape:  # class Shape(object)

    def __init__(self, x, y):
        self.x = x
        self.y = y

    def render(self, color):
        print(f'Rendered with color {color}')
        # ...

    def scale(self, scale_factor):
        print(f'Scaled with scale_factor {scale_factor}')
        # ...

    def square(self):
        print('Hello from square()')  # Good default impl?

    def centre(self):
        return self.x, self.y


class Circle(Shape):

    CONST = 3.14

    def __init__(self, x, y, radius):
        super().__init__(x, y)  # Shape.__init__(self, x, y)
        self.radius = radius

    def square(self):
        print(self.CONST * (int(self.radius) ** 2))


class Rectangle(Shape):

    def __init__(self, x, y, height, width):
        super().__init__(x, y)
        self.height = height
        self.width = width

    def print_sides(self):
        print(self.height, self.width)

    def square(self):
        print((int(self.height) * int(self.width)))


class Parallelogram(Rectangle):

    def __init__(self, x, y, height, width, angle):
        super().__init__(x, y, height, width)
        self.angle = angle

    def print_angle(self):
        print(self.angle)

    def square(self):
        print(int(self.width)*int(self.height))


# s = Shape(10, 20)
# print(s.x, s.y)
# s2 = Shape(11, 22)
# print(s2.x, s2.y)

r = Rectangle(1, 1, 10, 20)
r.square()

c = Circle(4, 2, 42)
c.square()

p = Parallelogram(1, 2, 10, 17, 60)
p.square()


print('--------------')
print('Task 2')


class Robots:

    def __init__(self, legs=False, head=False, tail=False):
        self.legs = legs
        self.head = head
        self.tail = tail

    def what_am_i(self):
        str_l = 'Hi, I\'m a robot. '
        if self.legs:
            str_l += 'I have legs. '
        if self.head:
            str_l += 'I have a head. '
        if self.tail:
            str_l += 'I have a tail.'
        if not self.legs and not self.head and not self.tail:
            str_l += 'I have no legs, no head and no tail :('
        print(str_l)


class SpotMini(Robots):
    def __init__(self, colour=None, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.colour = colour

    def define_spot_mini(self):
        print(f'Hi! I\'m SpotMini and I\'m a {str(self.colour)} robot! :)')


class Atlas(Robots):
    def __init__(self, work=None, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.work = work

    def what_i_do(self):
        print(f'Hi! I\'m Atlas and I\'m good at {str(self.work)}!!')


class Handle(Robots):
    def __init__(self, feature=None, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.feature = feature

    def my_feature(self):
        print(f'Hi! I\'m Handle and my unique feature is {str(self.feature)} !')


print('==========')
print('ROBOTS Test:')

rob = Robots()
rob.what_am_i()


print('==========')
print('SportMini Test:')

sm = SpotMini(legs=True, head=True, tail=True, colour='yellow')
sm.what_am_i()
sm.define_spot_mini()


print('==========')
print('Atlas Test:')
a = Atlas(legs=True, head=True, work='cooking')
a.what_am_i()
a.what_i_do()


print('==========')
print('Handle Test:')
h = Handle(tail=True, feature='moving things around')
h.what_am_i()
h.my_feature()
